<?php
/**
 * @package Dorayaki
 * @since Dorayaki 1.0
 */

get_header(); ?>

		<div id="site-content" class="fullwidth">
			
			<?php /* Start the Loop */ ?>
			<?php while ( have_posts() ) : the_post(); ?>

				<?php get_template_part( 'content', 'front-page' ); ?>


			<?php endwhile; // end of the loop. ?>

		</div><!-- end #site-content .fullwidth -->

<?php get_footer(); ?>