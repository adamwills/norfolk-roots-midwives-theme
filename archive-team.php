<?php
/**
 * The template for displaying Archive pages.
 *
 * @package Dorayaki
 * @since Dorayaki 1.0
 */

get_header(); ?>

	<div id="main-wrap">
		<div id="site-content">

		<?php if ( have_posts() ) : ?>

			<header class="entry-header">
				<h1 class="entry-title"><?php _e('Your Team', 'roots'); ?></h2>
			</header><!-- end .page-header -->

			<?php 

				$categories = array('midwives', 'second-attendants', 'administration', 'students');

				foreach ($categories as $cat ) :
				
					$args = array (
						'post_type'			=>	'team',
						'team-category'		=>	$cat,
						'order'				=>	'ASC',
						'orderby'			=>	'title',
						'posts_per_page'	=> -1
					);
					
					$query = new WP_Query( $args );

					if ( $query->found_posts > 0 ):

						$category = get_term_by( 'slug', $cat, 'team-category' );

						echo '<h2 class="team-category-title">' . $category->name . '</h2>';
					
						while ( $query->have_posts() ) : $query->the_post();

							get_template_part( 'content', 'team' );
						
						endwhile;

					endif;

					wp_reset_postdata();

				endforeach;

			?>

			<?php /* Display navigation to next/previous pages when applicable, also check if WP pagenavi plugin is activated */ ?>
			<?php if(function_exists('wp_pagenavi')) : wp_pagenavi(); else: ?>
				<?php dorayaki_content_nav( 'nav-below' ); ?>	
			<?php endif; ?>
			
			<?php else : ?>

				<article id="post-0" class="post no-results not-found">
					<header class="entry-header">
						<h1 class="entry-title"><?php _e( 'Nothing Found', 'dorayaki' ); ?></h1>
					</header><!-- .entry-header -->

					<div class="entry-content">
						<p><?php _e( 'It seems we can&rsquo;t find what you&rsquo;re looking for.', 'dorayaki' ); ?></p>
					</div><!-- .entry-content -->
				</article><!-- #post-0 -->

			<?php endif; ?>

		</div><!-- end #site-content -->

		<?php get_sidebar(); ?>
	</div><!-- end #main-wrap -->
<?php get_footer(); ?>