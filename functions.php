<?php
function theme_enqueue_styles() {
    wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );
}
add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles' );


remove_action('wp_head', 'print_emoji_detection_script', 7);
remove_action('wp_print_styles', 'print_emoji_styles');
add_image_size( 'roots_team_thumb', 600, 600, TRUE );
add_image_size( 'roots_featured_image', 800, 600 );

function roots_body_class( $body_classes ) {
    if (is_single() || is_page() && !is_front_page()) {
        if (!in_array(basename(get_permalink()), $body_classes)) {
          $body_classes[] = basename(get_permalink());
        }
      }

    if ( is_post_type_archive( array('team', 'events') ) ) {
        $body_classes[] = 'page';
    }
    return $body_classes;
}
add_filter( 'body_class', 'roots_body_class' );

function roots_limit_posts_per_archive_page() {
    if ( is_post_type_archive( array('team') ) ) :
        set_query_var('posts_per_archive_page', -1); // or use variable key: posts_per_page
        set_query_var('order', 'ASC');
    endif;

}
add_filter('pre_get_posts', 'roots_limit_posts_per_archive_page');

add_action( 'init', 'roots_team_init' );
function roots_team_init() {

    //* Create the labels (output) for the team
    $labels = array(
        'name'                  => _x( 'Team', 'post type general name', 'showcase' ),
        'singular_name'         => _x( 'Team Member', 'post type singular name', 'showcase' ),
        'menu_name'             => _x( 'Team', 'admin menu', 'showcase' ),
        'name_admin_bar'        => _x( 'Team Member', 'add new on admin bar', 'showcase' ),
        'add_new'               => _x( 'Add New', 'team', 'showcase' ),
        'add_new_item'          => __( 'Add New Team Member', 'showcase' ),
        'new_item'              => __( 'New Team Member', 'showcase' ),
        'edit_item'             => __( 'Edit Team Member', 'showcase' ),
        'view_item'             => __( 'View Team Member', 'showcase' ),
        'all_items'             => __( 'All Team Members', 'showcase' ),
        'search_items'          => __( 'Search Team Members', 'showcase' ),
        'parent_item_colon'     => __( ' ', 'showcase' ),
        'not_found'             => __( 'No team members found.', 'showcase' ),
        'not_found_in_trash'    => __( 'No team members found in Trash.', 'showcase' )
    );

    $args = array(
        'labels'                => $labels,
        'description'           => __( 'Team Members', 'showcase' ),
        'public'                => true,
        'publicly_queryable'    => true,
        'exclude_from_search'   => false,
        'show_ui'               => true,
        'query_var'             => true,
        'rewrite'               => array( 'slug' => 'team' ),
        'capability_type'       => 'post',
        'has_archive'           => true,
        'hierarchical'          => false,
        'menu_position'         => null,
        'menu_icon'             => 'dashicons-groups',
        'supports'              => array( 'title', 'editor', 'thumbnail', 'page-attributes' )
    );

    //* Register the post type
    register_post_type( 'team', $args );
}

// Register Custom Taxonomy
function roots_team_tax_init() {

    $labels = array(
        'name'                       => _x( 'Categories', 'Taxonomy General Name', 'showcase' ),
        'singular_name'              => _x( 'Category', 'Taxonomy Singular Name', 'showcase' ),
        'menu_name'                  => __( 'Categories', 'showcase' ),
        'all_items'                  => __( 'All Items', 'showcase' ),
        'parent_item'                => __( 'Parent Item', 'showcase' ),
        'parent_item_colon'          => __( 'Parent Item:', 'showcase' ),
        'new_item_name'              => __( 'New Category', 'showcase' ),
        'add_new_item'               => __( 'Add New Item', 'showcase' ),
        'edit_item'                  => __( 'Edit Item', 'showcase' ),
        'update_item'                => __( 'Update Item', 'showcase' ),
        'view_item'                  => __( 'View Item', 'showcase' ),
        'separate_items_with_commas' => __( 'Separate items with commas', 'showcase' ),
        'add_or_remove_items'        => __( 'Add or remove items', 'showcase' ),
        'choose_from_most_used'      => __( 'Choose from the most used', 'showcase' ),
        'popular_items'              => __( 'Popular Items', 'showcase' ),
        'search_items'               => __( 'Search Items', 'showcase' ),
        'not_found'                  => __( 'Not Found', 'showcase' ),
        'no_terms'                   => __( 'No items', 'showcase' ),
        'items_list'                 => __( 'Items list', 'showcase' ),
        'items_list_navigation'      => __( 'Items list navigation', 'showcase' ),
    );
    $args = array(
        'labels'                     => $labels,
        'hierarchical'               => true,
        'public'                     => true,
        'show_ui'                    => true,
        'show_admin_column'          => true,
        'show_in_nav_menus'          => true,
        'show_tagcloud'              => false,
    );
    register_taxonomy( 'team-category', array( 'team' ), $args );

}
add_action( 'init', 'roots_team_tax_init', 0 );

function roots_list_events( $number = 3 ) {
    global $post;
    $posts = get_posts( array(
        'post_type' => 'tribe_events',
        'posts_per_page' => $number,
        'order' =>  'ASC'

    ) );

    if ($posts) :
        $output = '<ul class="front-page-events">';
        foreach( $posts as $post ) :
            setup_postdata( $post );
            $title = get_the_title();
            $output .= '<li>';
            $output .= '<a href="'. tribe_get_event_link() .'" class="event-title">' . $title . '</a>';
            $output .= '<div class="event-date">' . tribe_events_event_schedule_details() . '</div>';
            $output .= '<div class="event-ventue">' . tribe_get_venue() . '</div>';
            $output .= '<div class="event-description">' . get_the_excerpt() . '</div>';
            $output .= '<a href="'. tribe_get_event_link() .'" class="standard-btn green-btn">' . sprintf( __('Learn more<span class="sr-only"> about %s </span>', 'sage'), $title ) . '</a>';
            $output .= '</li>';
        endforeach;
        $output.='</ul>';

        $output.='<p class="link-all-events"><a href="/event/">'. __('View All Events', 'roots') .'</a></p>';
    else:
        $output = '<p style="text-align: center">There are no upcoming events currently scheduled.</p>';
    endif;
    wp_reset_query();
    echo $output;

}

function roots_add_homepage_background() {

    if ( is_front_page() && has_post_thumbnail() ) :

        $thumb_id = get_post_thumbnail_id();
        $img = wp_get_attachment_image_src($thumb_id, 'medium_large', true);
        $small = $img[0];

        $img = wp_get_attachment_image_src($thumb_id, 'large', true);
        $medium = $img[0];

        $img = wp_get_attachment_image_src($thumb_id, 'roots_featured_image', true);
        $large = $img[0];


        ?>
        <style>
        .section-intro {
            background-image: url(<?= $small; ?>);
        }

        @media screen and (min-width: 768px) {
            .section-intro {
                background-image: url(<?= $medium; ?>);
            }
        }

        @media screen and (min-width: 1200px) {
            .section-intro {
                background-image: url(<?= $large; ?>);
            }
        }
        </style>
    <?php
    endif;
}
add_action( 'wp_head', 'roots_add_homepage_background');

function roots_display_team() {

    $loop = new WP_Query( array(
            'posts_per_page' => -1,
            'post_type'      => 'team',
            'order'          => 'ASC',
            'order_by'       => 'menu_order'
        ) );

        if( $loop->have_posts() ) : ?>

        <div class="team-grid">

            <?php while( $loop->have_posts() ) : $loop->the_post(); ?>

                <div class="team-member">
                    <a href="<?php the_permalink(); ?>" title="<?php echo esc_attr( sprintf( __( 'Permalink to %s', 'roots' ), the_title_attribute( 'echo=0' ) ) ); ?>" rel="bookmark">
                        <div class="overlay">
                            <div class="overlay-inner">
                                <div class="overlay-details">
                                    <?php the_title( '<h3 class="team-member-name">', '</h3>' );?>
                                    <span>View Profile</span>
                                </div>
                            </div>
                        </div>
                        <?php if(has_post_thumbnail()) : ?>
                        <div class="team-member-image">
                            <?php the_post_thumbnail( 'roots_team_thumb' ); ?>
                        </div>
                    <?php endif; ?>
                    </a>
                </div>
            <?php endwhile; ?>

            </div>

        <?php endif;

    wp_reset_query();
}
