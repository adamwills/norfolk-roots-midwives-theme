<div class="section-intro">
	
	<div class="section-wrap">

		<?php get_template_part( 'content', 'page' ); ?>

	</div>

</div>

<div class="section-info">
	
	<div class="section-wrap">

		<h2><?php _e('About Us', 'roots'); ?></h2>

		<div class="flex-columns">
		
			<div class="flex-column">
				<?php the_field('intro_column_1'); ?>
			</div>

			<div class="flex-column">
				<?php the_field('intro_column_2'); ?>
			</div>

			<div class="flex-column">
				<?php the_field('intro_column_3'); ?>
			</div>

		</div>


	</div>

</div>

<div class="section-events">
	
	<div class="section-wrap">
		
		<h2><?php _e('Upcoming Events', 'roots'); ?></h2>

		<?php roots_list_events(); ?>

	</div>

</div>